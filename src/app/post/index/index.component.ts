import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Post } from '../post';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {
  posts: Post[] = [];


  constructor(public appService:AppService) { }

  ngOnInit(): void {


    this.appService.getAll().subscribe((data: Post[])=>{
      this.posts = data;
      console.log(this.posts);
    })  
  }
  
  deletePost(id){
    this.appService.delete(id).subscribe(res => {
         this.posts = this.posts.filter(item => item.id !== id);
         console.log('Post deleted successfully!');
    })
  }
  
}
  
