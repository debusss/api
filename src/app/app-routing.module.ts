import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostModule } from './post/post.module';
const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule,PostModule]
})
export class AppRoutingModule { }
